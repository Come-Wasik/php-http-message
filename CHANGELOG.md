# Changelog

All notable changes to this project will be documented in this file, in reverse chronological order by release.

## 1.2.0 - 2020-11-02

Update all classes/interfaces documentation.
Add the nolikein/cookie-manager lirairy in the project to use cookies with it.
Fixing the ResponseTest class name.
Update phpunit.xml: remove bootstrap file + disable the test cache file.

## 1.1.1 - 2020-10-26
Update the package "nolikein/stream" to "nolikein/stream-manager".

## 1.1.0 - 2020-10-25

Changing the namespace of all classes from "Nolikein/Component/Http" to "Nolikein/HttpMessage".

## 1.0.2 - 2020-10-25

Add a changelog.md file
Update the file README.MD : Add link to packagist.

## 1.0.1 - 2020-10-25

Moving the directory tests/functional/http to tests/http.
This is because the test is not named a functional test but a unitary test. by the way,
there is none functional test.

## 1.0.0 - 2020-10-24

Initial stable release.