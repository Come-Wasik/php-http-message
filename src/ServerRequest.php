<?php

namespace Nolikein\HttpMessage;

use Nolikein\Cookie\CookieInterface;
use Nolikein\HttpMessage\Exception\InvalidArgumentException;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\UploadedFileInterface;

/**
 * ServerRequest is a class which represent an http server
 * request. It implements the Psr\ServerRequestInterface.
 * 
 * @author Côme Wasik <wasik.come@gmail.com>
 * @license https://opensource.org/licenses/MIT MIT
 */
class ServerRequest extends Request implements ServerRequestInterface
{
    /** @var string[] $bodyParam May be post data */
    protected $postData = null;

    /** @var string[] $cookies The cookie list */
    protected $cookies = [];

    /** @var string[] $serverData The server data */
    protected $serverData = [];

    /** @var UploadedFile $uploadedFiles The list of uploaded files */
    protected $uploadedFiles = [];

    /** @var mixed[] $attributes Additionnal attributes link to the server request */
    protected $attributes = [];

    public function __construct()
    {
        parent::__construct();
    }

    /* Server params */
    public function getServerParams()
    {
        return $this->serverData;
    }

    /**
     * Return a new instance of this filled with the server params passed as argument
     *
     * @param string[] $serverParams
     */
    public function withServerParams(array $serverParams)
    {
        $clone = clone $this;
        foreach ($serverParams as $key => $value) {
            if (!is_string($key) || !is_string($value)) {
                throw new InvalidArgumentException('The server data MUST be string', 500);
            }
        }
        $clone->serverData = $serverParams;
        return $clone;
    }

    /* Cookie params */
    public function getCookieParams()
    {
        return $this->cookies;
    }

    public function withCookieParams(array $cookies): ServerRequest
    {
        $clone = clone $this;
        # Checking the cookie struct
        foreach ($cookies as $cookie) {
            if(!($cookie instanceof CookieInterface)) {
                throw new InvalidArgumentException('The cookie "' . serialize($cookie) . '" and the value is not valid', 500);
            }
        }
        # Affect new values
        $clone->cookies = $cookies;
        return $clone;
    }

    /* Query params */
    public function getQueryParams()
    {
        parse_str($this->getUri()->getQuery(), $output);
        return $output;
    }

    public function withQueryParams(array $query): ServerRequest
    {
        $clone = clone $this;
        # Affect new value
        $clone = $clone->withUri($clone->getUri()->withQuery($this->parseQuery($query)));
        return $clone;
    }

    /**
     * Parse a query form array to string
     *
     * The result for an array like ['a' => 'b', 'c' => 'd'] is 'a=b&c=d'
     *
     * @param string[] $query Contains query in the form of key/value
     * @return string The parsed query
     */
    private function parseQuery(array $query): string
    {
        $parsedQuery = [];
        foreach ($query as $key => $name) {
            $parsedQuery[] = $key . '=' . $name;
        }
        return implode('&', $parsedQuery);
    }

    /* Upload files */
    public function getUploadedFiles()
    {
        return $this->uploadedFiles;
    }

    public function withUploadedFiles(array $uploadedFiles): ServerRequest
    {
        foreach ($uploadedFiles as $file) {
            if (($file instanceof UploadedFileInterface) === false) {
                throw new InvalidArgumentException('One uploaded file is not an instance of ' . UploadedFileInterface::class, 500);
            }
        }
        $clone = clone $this;
        $clone->uploadedFiles = $uploadedFiles;
        return $clone;
    }

    /* Parsed body methods */
    public function getParsedBody()
    {
        return $this->bodyParam;
    }

    public function withParsedBody($data): ServerRequest
    {
        $clone = clone $this;

        if (is_array($data)) {
            $clone->bodyParam = $data;
        } elseif (is_string($data) && ($decodedJson = json_decode($data)) !== null) {
            $clone->bodyParam = $decodedJson;
        } elseif (is_object($data) && method_exists($data, '__toString')) {
            ob_start();
            echo $data;
            $clone->bodyParam = ob_get_clean();
        } else {
            throw new InvalidArgumentException('An insupported argument type was provided', 500);
        }
        return $clone;
    }

    /* Attribute methods */
    public function getAttributes()
    {
        return $this->attributes;
    }

    public function getAttribute($name, $default = null)
    {
        if (key_exists($name, $this->attributes)) {
            return $this->attributes[$name];
        } else {
            return $default;
        }
    }

    public function withAttribute($name, $value): ServerRequest
    {
        $clone = clone $this;
        $clone->attributes[$name] = $value;
        return $clone;
    }

    public function withoutAttribute($name): ServerRequest
    {
        $clone = clone $this;
        unset($clone->attributes[$name]);
        return $clone;
    }
}
