<?php

namespace Nolikein\HttpMessage;

use Nolikein\HttpMessage\Exception\InvalidArgumentException;
use Psr\Http\Message\ResponseInterface;

/**
 * Response is a class which represent an http response.
 * It implements the Psr\ResponseInterface.
 * 
 * @author Côme Wasik <wasik.come@gmail.com>
 * @license https://opensource.org/licenses/MIT MIT
 */
class Response extends Message implements ResponseInterface
{
    /** @var int $statusCode The status code from the response */
    protected $statusCode = 200;

    /** @var string $reasonPhrase The reason phrase associated with the status code */
    protected $reasonPhrase = '';

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function withStatus($code, $reasonPhrase = '')
    {
        $clone = clone $this;

        if (!\is_int($code)) {
            throw new InvalidArgumentException('The code must be an int', 500);
        }

        if ($code < 100 || $code > 999) {
            throw new InvalidArgumentException('The code must be a 3-digit number', 500);
        }

        if (!\is_string($reasonPhrase)) {
            throw new InvalidArgumentException('The reason phrase must be a string', 500);
        }

        $clone->statusCode = $code;
        $clone->reasonPhrase = $reasonPhrase;

        return $clone;
    }

    public function getReasonPhrase(): string
    {
        return $this->reasonPhrase;
    }
}
