<?php

namespace Nolikein\HttpMessage\Factory;

use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\RequestInterface;
use Nolikein\HttpMessage\Exception\InvalidArgumentException;
use Nolikein\HttpMessage\Request;
use Psr\Http\Message\UriInterface;

/**
 * Request Factory allows to create an http request.
 * It implements the Psr\RequestFactoryInterface.
 * 
 * @author Côme Wasik <wasik.come@gmail.com>
 * @license https://opensource.org/licenses/MIT MIT
 */
class RequestFactory implements RequestFactoryInterface
{
    public function createRequest(string $method, $uri): RequestInterface
    {
        if (($uri instanceof UriInterface) === false && !is_string($uri)) {
            throw new InvalidArgumentException('The uri param of a RequestFactory MUST be a string or a ' . UriInterface::class . ' object');
        }

        $request = new Request();
        $uriFactory = new UriFactory();
        $request = $request->withUri(($uri instanceof UriInterface ? $uri : ($uriFactory->createUri($uri))));
        $request = $request->withMethod($method)
            ->withProtocolVersion('1.1');

        # Finding Target method
        if ($method === Request::METHOD_OPTION && $request->getUri()->getPath() === '*') {
            $request = $request->withRequestTarget(Request::TARGET_ASTERIX);
        } elseif ($method === Request::METHOD_CONNECT) {
            $request = $request->withRequestTarget(Request::TARGET_AUTHORITY);
        } else {
            $request = $request->withRequestTarget(Request::TARGET_ORIGIN);
        }
        return $request;
    }
}
