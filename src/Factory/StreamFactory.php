<?php

namespace Nolikein\HttpMessage\Factory;

use Psr\Http\Message\StreamFactoryInterface;
use Nolikein\Stream\StreamFactory as BaseStreamFactory;

/**
 * Stream Factory allows to create a stream.
 * It implements the Psr\StreamFactoryInterface.
 * 
 * @author Côme Wasik <wasik.come@gmail.com>
 * @license https://opensource.org/licenses/MIT MIT
 */
class StreamFactory extends BaseStreamFactory implements StreamFactoryInterface
{
    
}