<?php

namespace Nolikein\HttpMessage\Factory;

use Psr\Http\Message\ServerRequestFactoryInterface;
use Psr\Http\Message\ServerRequestInterface;
use Nolikein\HttpMessage\ServerRequest;

/**
 * Server Request Factory allows to create an http server request.
 * It implements the Psr\ServerRequestFactoryInterface.
 * 
 * @author Côme Wasik <wasik.come@gmail.com>
 * @license https://opensource.org/licenses/MIT MIT
 */
class ServerRequestFactory implements ServerRequestFactoryInterface
{
    public function createServerRequest(string $method, $uri, array $serverParams = []): ServerRequestInterface
    {
        $requestFactory = new RequestFactory();
        $request = $requestFactory->createRequest($method, $uri);
        $serverRequest = new ServerRequest();
        $serverRequest = $serverRequest
            ->withServerParams($serverParams)
            ->withUri($request->getUri())
            ->withMethod($request->getMethod())
            ->withProtocolVersion($request->getProtocolVersion());
        
        return $serverRequest;
    }
}