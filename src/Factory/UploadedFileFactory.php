<?php

namespace Nolikein\HttpMessage\Factory;

use Psr\Http\Message\StreamInterface;
use Psr\Http\Message\UploadedFileFactoryInterface;
use Psr\Http\Message\UploadedFileInterface;
use Nolikein\HttpMessage\Exception\InvalidArgumentException;
use Nolikein\HttpMessage\UploadedFile;

/**
 * Uploaded File Factory allows to create an uploaded file.
 * It implements the Psr\UploadedFileFactoryInterface.
 * 
 * @author Côme Wasik <wasik.come@gmail.com>
 * @license https://opensource.org/licenses/MIT MIT
 */
class UploadedFileFactory implements UploadedFileFactoryInterface
{
    public function createUploadedFile(
        StreamInterface $stream,
        int $size = null,
        int $error = \UPLOAD_ERR_OK,
        string $clientFilename = null,
        string $clientMediaType = null
    ): UploadedFileInterface
    {
        if(!$stream->isReadable()) {
            throw new InvalidArgumentException('The uploaded file is not readable');
        }

        $fakeFile = [
            'name' => $clientFilename,
            'type' => $clientMediaType,
            'size' => $size,
            'tmp_name' => $stream->getMetadata('uri'),
            'error' => $error
        ];
        $fileUpload = new UploadedFile();
        $fileUpload->setFile($fakeFile);
        return $fileUpload;
    }
}