<?php

namespace Nolikein\HttpMessage\Factory;

use Psr\Http\Message\UriFactoryInterface;
use Psr\Http\Message\UriInterface;
use Nolikein\HttpMessage\Uri;
use Nolikein\HttpMessage\Exception\InvalidArgumentException;

/**
 * Uri Factory allows to create an uri.
 * It implements the Psr\UriFactoryInterface.
 * 
 * @author Côme Wasik <wasik.come@gmail.com>
 * @license https://opensource.org/licenses/MIT MIT
 */
class UriFactory implements UriFactoryInterface
{
    public function createUri(string $uri = ''): UriInterface
    {
        $uriComponent = new Uri();
        $uriParts = parse_url($uri);

        if (empty($uriParts)) {
            throw new InvalidArgumentException('The uri MUST NOT be empty');
        }

        if (key_exists('scheme', $uriParts)) {
            $uriComponent = $uriComponent->withScheme($uriParts['scheme']);
        } else {
            # Force http protocol as scheme value if this last is not defined
            $uriComponent = $uriComponent->withScheme('http');
        }
        if (key_exists('host', $uriParts)) {
            $uriComponent = $uriComponent->withHost($uriParts['host']);
        }
        if (key_exists('user', $uriParts)) {
            $uriComponent = $uriComponent->withUserInfo(
                $uriParts['user'],
                (key_exists('pass', $uriParts) ? $uriParts['pass'] : null)
            );
        }
        if (key_exists('port', $uriParts)) {
            $uriComponent = $uriComponent->withPort((int) $uriParts['port']);
        }
        if (key_exists('path', $uriParts)) {
            $uriComponent = $uriComponent->withPath($uriParts['path']);
        } else {
            $uriComponent = $uriComponent->withPath('/');
        }
        if (key_exists('query', $uriParts)) {
            $uriComponent = $uriComponent->withQuery($uriParts['query']);
        }
        if (key_exists('fragment', $uriParts)) {
            $uriComponent = $uriComponent->withFragment($uriParts['fragment']);
        }
        return $uriComponent;
    }
}
