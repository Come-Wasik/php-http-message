<?php

namespace Nolikein\HttpMessage;

use Nolikein\HttpMessage\Exception\InvalidArgumentException;
use Psr\Http\Message\MessageInterface;
use Psr\Http\Message\StreamInterface;
use Nolikein\HttpMessage\Exception\HeaderNotFoundException;
use Nolikein\HttpMessage\Exception\InvalidProtocolVersionException;
use Nolikein\HttpMessage\Exception\ProtocolVersionNotDefinedException;

/**
 * Message is a class which represent an http message.
 * It implements the Psr\MessageInterface.
 * 
 * Maybe you will be interrested by Request, ServerRequest
 * or Response objects which implement it.
 * 
 * @author Côme Wasik <wasik.come@gmail.com>
 * @license https://opensource.org/licenses/MIT MIT
 */
class Message implements MessageInterface
{
    /** @var string $protocolVersion The protocol version of the current message */
    protected $protocolVersion = null;

    /** @var string[][] $headers The complete list of all headers */
    protected $headers = [];

    /** @var StreamInterface $body The message body */
    protected $body = null;

    /**
     * @inheritDoc
     *
     * @throws ProtocolVersionNotDefinedException When try to get the protocol version whereas it is not defined
     */
    public function getProtocolVersion(): ?string
    {
        if ($this->protocolVersion === null) {
            return null;
        }

        return $this->protocolVersion;
    }

    /**
     * @inheritDoc
     */
    public function withProtocolVersion($version)
    {
        $clone = clone $this;

        if (!\is_string($version) && !\is_float($version)) {
            throw new InvalidArgumentException('The protocol version is neither a float or a string', 500);
        }

        if (filter_var($version, FILTER_VALIDATE_FLOAT) === false) {
            throw new InvalidProtocolVersionException($version);
        }

        // Clean 0 unused (like in 01.01)
        $majorVersion = (int) substr($version, 0, strpos($version, '.'));
        $minorVersion = (int) substr($version, strpos($version, '.') + 1);

        $clone->protocolVersion = (string) $majorVersion . '.' . $minorVersion;

        return $clone;
    }

    /**
     * @inheritDoc
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @inheritDoc
     */
    public function hasHeader($name): bool
    {
        /** @var string $insensitiveHeaderName A header name with insensitive case */
        $insensitiveHeaderName = \strtolower($name);

        if (key_exists($insensitiveHeaderName, $this->headers)) {
            return true;
        }
        return false;
    }

    /**
     * @inheritDoc
     */
    public function getHeader($name)
    {
        /** @var string $insensitiveHeaderName A header name with insensitive case */
        $insensitiveHeaderName = \strtolower($name);

        if (!key_exists($insensitiveHeaderName, array_change_key_case($this->headers))) {
            throw new HeaderNotFoundException($name);
        }

        $valueExtracted = $this->headers[$insensitiveHeaderName];

        if (is_array($valueExtracted)) {
            return $valueExtracted;
        } else {
            return [$valueExtracted];
        }
    }

    /**
     * @inheritDoc
     */
    public function getHeaderLine($name)
    {
        /** @var string $insensitiveHeaderName A header name with insensitive case */
        $insensitiveHeaderName = \strtolower($name);

        if (!key_exists($insensitiveHeaderName, $this->headers)) {
            throw new HeaderNotFoundException($name);
        }

        return \implode('; ', $this->headers[$insensitiveHeaderName]);
    }

    /**
     * @inheritDoc
     */
    public function withHeader($name, $value)
    {
        $clone = clone $this;

        /** @var string $insensitiveHeaderName A header name with insensitive case */
        $insensitiveHeaderName = \strtolower($name);

        if (!\is_string($name)) {
            throw new InvalidArgumentException('The header that you want to set has not a name with string type', 500);
        }

        if (!\is_string($value) && !\is_array($value)) {
            throw new InvalidArgumentException('The header named "' . $name . '" that you want to set is not of type string or array', 500);
        }

        if (\is_string($value)) {
            $clone->headers[$insensitiveHeaderName] = [$value];
        } else {
            $clone->headers[$insensitiveHeaderName] = $value;
        }

        return $clone;
    }

    /**
     * @inheritDoc
     */
    public function withAddedHeader($name, $value)
    {
        $clone = clone $this;

        /** @var string $insensitiveHeaderName A header name with insensitive case */
        $insensitiveHeaderName = \strtolower($name);

        if (!\is_string($name)) {
            throw new InvalidArgumentException('The header that you want to set has not a name with string type', 500);
        }

        if (!\is_string($value) && !\is_array($value)) {
            throw new InvalidArgumentException('The header named "' . $name . '" that you want to set is not of type string or array', 500);
        }

        $clone->headers[$insensitiveHeaderName][] = $value;

        return $clone;
    }

    /**
     * @inheritDoc
     */
    public function withoutHeader($name)
    {
        $clone = clone $this;

        /** @var string $insensitiveHeaderName A header name with insensitive case */
        $insensitiveHeaderName = \strtolower($name);

        if (key_exists($insensitiveHeaderName, $clone->headers)) {
            unset($clone->headers[$insensitiveHeaderName]);
        }

        return $clone;
    }

    /**
     * @inheritDoc
     *
     * @return StreamInterface|null Returns the body as a stream.
     */
    public function getBody(): ?StreamInterface
    {
        return $this->body;
    }

    /**
     * @inheritDoc
     */
    public function withBody(StreamInterface $body)
    {
        $clone = clone $this;

        $clone->body = $body;

        return $clone;
    }
}
