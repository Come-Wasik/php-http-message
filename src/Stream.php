<?php

namespace Nolikein\HttpMessage;

use Psr\Http\Message\StreamInterface;
use Nolikein\Stream\Stream as BaseStream;

/**
 * Stream is a class which represents a stream.
 * It implements the Psr\StreamInterface.
 * 
 * This class use an external librairy due to
 * a need of decentrilize the feature.
 * 
 * @author Côme Wasik <wasik.come@gmail.com>
 * @license https://opensource.org/licenses/MIT MIT
 */
class Stream extends BaseStream implements StreamInterface
{
}
