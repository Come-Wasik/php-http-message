<?php

namespace Nolikein\HttpMessage;

use Psr\Http\Message\UploadedFileInterface;
use Nolikein\HttpMessage\Exception\RuntimeException;

/**
 * UploadedFile is a class which represents a file
 * uploaded from a web browser to a web server.
 * It implements the Psr\UploadedFileInterface.
 * 
 * It allows you to get data from a file, or moving it.
 * 
 * @author Côme Wasik <wasik.come@gmail.com>
 * @license https://opensource.org/licenses/MIT MIT
 */
class UploadedFile implements UploadedFileInterface
{
    /** @var mixed[] $uploadedFile The original file data which are uploaded */
    protected $uploadedFile = [];

    /**
     * Allow to set a file even after the object is created
     *
     * @param array uploadedFile An uploadedfile contained in an array
     */
    public function setFile(array $uploadedFile = null): void
    {
        $this->uploadedFile = $uploadedFile;
    }

    public function getStream(): Stream
    {
        if (empty($this->uploadedFile) || !key_exists('tmp_name', $this->uploadedFile)) {
            throw new RuntimeException('No stream available', 500);
        }
        if (!file_exists($this->uploadedFile['tmp_name'])) {
            throw new RuntimeException('The file does not exist anymore', 500);
        }
        $streamOpen = fopen($this->uploadedFile['tmp_name'], 'r');
        if ($streamOpen === null) {
            throw new RuntimeException('The source file cannot be oppened', 500);
        }
        return new Stream($streamOpen);
    }

    public function moveTo($targetPath): void
    {
        # Opening the stream from the source file
        if (empty($this->uploadedFile) || !key_exists('tmp_name', $this->uploadedFile)) {
            throw new RuntimeException('No stream available', 500);
        }

        if (!file_exists($this->uploadedFile['tmp_name'])) {
            throw new RuntimeException('The source file does not exists', 500);
        }

        $sourceStream = fopen($this->uploadedFile['tmp_name'], 'r');
        if ($sourceStream === false) {
            throw new RuntimeException('The source file cannot be open. Maybe it was called already', 500);
        }

        # Deleting the destination file if has existed
        if (file_exists($targetPath)) {
            $success = unlink($targetPath);
            if (!$success) {
                throw new RuntimeException('The file cannot be deleted', 500);
            }
        }

        # Moving the content from the source file to the destination file
        $destStream = fopen($targetPath, 'w+');
        if ($destStream === false) {
            throw new RuntimeException('The destination file cannot be open', 500);
        }
        $byteCopied = stream_copy_to_stream($sourceStream, $destStream);
        if ($byteCopied === false) {
            throw new RuntimeException('The uploaded file could not be moved. Error during the process', 500);
        }

        # Closing stream and deletion of the source file
        if (fclose($sourceStream) === false) {
            throw new RuntimeException('The source file could not be closed', 500);
        }
        $success = unlink($this->uploadedFile['tmp_name']);
        if (!$success) {
            throw new RuntimeException('The source file cound not be deleted', 500);
        }
        if (fclose($destStream) === false) {
            throw new RuntimeException('The destination file could not be closed', 500);
        }
    }

    public function getSize(): ?int
    {
        return $this->uploadedFile['size'] ?? null;
    }

    public function getError(): ?int
    {
        return $this->uploadedFile['error'] ?? null;
    }

    public function getClientFilename(): ?string
    {
        return $this->uploadedFile['name'] ?? null;
    }

    public function getClientMediaType(): ?string
    {
        return $this->uploadedFile['type'] ?? null;
    }
}
