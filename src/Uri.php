<?php

declare(strict_types=1);

namespace Nolikein\HttpMessage;

use Nolikein\HttpMessage\Exception\InvalidArgumentException;
use Psr\Http\Message\UriInterface;

/**
 * Uri is a class which represents a Uri.
 * It implements the Psr\UriInterface.
 * 
 * @author Côme Wasik <wasik.come@gmail.com>
 * @license https://opensource.org/licenses/MIT MIT
 */
class Uri implements UriInterface
{
    const SUPPORTED_SCHEMES = [
        'http' => [
            'port' => 80
        ],
        'https' => [
            'port' => 443
        ]
    ];

    /** @var string $scheme The protocol/schema part of this uri */
    protected $scheme = '';

    /** @var string $userInfo The user name */
    protected $username = '';

    /** @var string $password The user password */
    protected $password = '';

    /** @var mixed $host An address (ip or DNS) to a serveur */
    protected $host = '';

    /** @var int $port The current port used to access to this uri */
    protected $port = null;

    /** @var string $path The path into the host */
    protected $path = '';

    /** @var string $query The arguments of a GET request */
    protected $query = '';

    /** @var string $fragment The # part at the end of a uri */
    protected $fragment = '';

    public function getScheme(): string
    {
        return strtolower($this->scheme);
    }

    public function getAuthority(): string
    {
        $authority = '';

        if (!empty($this->username)) {
            if (empty($this->password)) {
                $authority .= $this->username . '@';
            } else {
                $authority .= $this->username . ':' . $this->password . '@';
            }
        }
        $authority .= $this->host;

        if ($this->getPort() !== null) {
            $authority .= (string) ':' . $this->getPort();
        }

        return $authority;
    }

    public function getUserInfo(): string
    {
        return (!empty($this->password) ? $this->username . ':' . $this->password : $this->username);
    }

    public function getHost(): string
    {
        return strtolower($this->host);
    }

    public function getPort(): ?int
    {
        if (
            $this->port !== null
            && empty($this->scheme)
            || (
                !empty($this->scheme)
                && $this->port !== self::SUPPORTED_SCHEMES[$this->scheme]['port']
            )) {
            return $this->port;
        }
        return null;
    }

    public function getPath()
    {
        $path = rawurlencode($this->path);
        $path = str_replace('%2F', '/', $path);
        return $path;
    }

    public function getQuery()
    {
        $query = rawurlencode($this->query);
        $query = str_replace('%3D', '=', $query);
        $query = str_replace('%26', '&', $query);
        return $query;
    }

    public function getFragment()
    {
        return rawurlencode($this->fragment);
    }

    public function withScheme($scheme)
    {
        $clone = clone $this;

        if (!\is_string($scheme)) {
            throw new InvalidArgumentException('The scheme MUST be a string', 500);
        }

        /** @var string[] $supportedShemeList The list of all supported schemes */
        $supportedShemeList = array_keys(self::SUPPORTED_SCHEMES);

        if (!\in_array(strtolower($scheme), ['' => ''] + $supportedShemeList)) {
            throw new InvalidArgumentException('Scheme not supported. Please use : ' . implode(', ', $supportedShemeList), 500);
        }

        $clone->scheme = $scheme;
        return $clone;
    }

    /**
     * @throws InvalidArgumentException The username is not a string
     * @throws InvalidArgumentException The password is not a string or null
     */
    public function withUserInfo($user, $password = null)
    {
        $clone = clone $this;

        if (!\is_string($user)) {
            throw new InvalidArgumentException('The username MUST be a string', 500);
        }

        if (!\is_string($password) && !\is_null($password)) {
            throw new InvalidArgumentException('The password MUST be a string or null if is there is no password', 500);
        }

        $clone->username = $user;
        if (!\is_null($password)) {
            $clone->password = $password;
        }

        return $clone;
    }

    public function withHost($host)
    {
        $clone = clone $this;

        if (!\is_string($host)) {
            throw new InvalidArgumentException('The host MUST be a string', 500);
        }

        if (empty($host)) {
            $clone->host = $host;
        } else {
            if (
                filter_var($host, FILTER_VALIDATE_IP) === false
                && filter_var($host, FILTER_VALIDATE_DOMAIN, ['flags' => FILTER_FLAG_HOSTNAME]) === false
            ) {
                throw new InvalidArgumentException('The host MUST be a valid ip address or a domain name', 500);
            }

            $clone->host = strtolower($host);
        }

        return $clone;
    }

    public function withPort($port)
    {
        $clone = clone $this;

        if (!\is_int($port) && !is_null($port)) {
            throw new InvalidArgumentException('The port MUST be an int or null for nothing', 500);
        }

        if (is_int($port)) {
            if ($port < 0 || $port > 65535) {
                throw new InvalidArgumentException('The port MUST be between 1 and 65535', 500);
            }
        }

        $clone->port = $port;
        return $clone;
    }

    public function withPath($path)
    {
        $clone = clone $this;

        if (!\is_string($path)) {
            throw new InvalidArgumentException('The path "' . $path . '" MUST be a string', 500);
        }

        // Test path by comparing the sanitized param path with the param path
        $supposedGoodPath = filter_var($path, FILTER_SANITIZE_URL);
        if ($path !== $supposedGoodPath) {
            throw new InvalidArgumentException('The path "' . $path . '" MUST be a valid part of a url', 500);
        }

        $clone->path = $path;
        return $clone;
    }

    public function withQuery($query)
    {
        $clone = clone $this;

        if (!\is_string($query)) {
            throw new InvalidArgumentException('The query "' . $query . '" MUST be a string', 500);
        }

        $equalQuantity = substr_count($query, '=');
        $andQuantity = substr_count($query, '&');

        if (!empty($query) && substr_count($query, '=') == 0) {
            throw new InvalidArgumentException('The query "' . $query . '" if not empty MUST contain at least one valid argument', 500);
        }

        if ($andQuantity >= 1 && $andQuantity >= $equalQuantity) {
            throw new InvalidArgumentException('The query "' . $query . '" must contain valid arguments after a &', 500);
        }

        $clone->query = $query;
        return $clone;
    }

    public function withFragment($fragment)
    {
        $clone = clone $this;

        if (!\is_string($fragment)) {
            throw new InvalidArgumentException('The query "' . $fragment . '" MUST be a string', 500);
        }

        $clone->fragment = $fragment;
        return $clone;
    }

    public function __toString()
    {
        $uri = '';

        if (!empty($this->getScheme())) {
            $uri .= $this->getScheme() . ':';
        }

        if (!empty($this->getAuthority())) {
            $uri .= '//' . $this->getAuthority();
        }

        if (!empty($this->path)) {
            if ($this->getPath()[0] !== '/' && !empty($this->getAuthority())) {
                $uri .= '/' . $this->getPath();
            } elseif ($this->getPath()[0] === '/' && empty($this->getAuthority())) {
                $uri .= '/' . ltrim($this->getPath(), '/');
            } else {
                $uri .= $this->getPath();
            }
        }

        if (!empty($this->getQuery())) {
            $uri .= '?' . $this->getQuery();
        }

        if (!empty($this->getFragment())) {
            $uri .= '#' . $this->getFragment();
        }
        return $uri;
    }
}
