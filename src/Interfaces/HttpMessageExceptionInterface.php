<?php

namespace Nolikein\HttpMessage\Interfaces;

/**
 * This interface can be use in catch statement to catch
 * only exceptions which implements the interface.
 * 
 * @author Côme Wasik <wasik.come@gmail.com>
 * @license https://opensource.org/licenses/MIT MIT
 */
interface HttpMessageExceptionInterface
{
}
