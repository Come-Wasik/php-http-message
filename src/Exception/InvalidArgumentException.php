<?php

namespace Nolikein\HttpMessage\Exception;

use Nolikein\HttpMessage\Interfaces\HttpMessageExceptionInterface;

/**
 * This exception can be used to catch more accurately
 * an invalid argument exception thrown from a HttpMessage
 * object.
 * 
 * @author Côme Wasik <wasik.come@gmail.com>
 * @license https://opensource.org/licenses/MIT MIT
 */
class InvalidArgumentException extends \InvalidArgumentException implements HttpMessageExceptionInterface
{

}
