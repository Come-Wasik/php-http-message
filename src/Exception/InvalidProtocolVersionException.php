<?php

namespace Nolikein\HttpMessage\Exception;

use Nolikein\HttpMessage\Interfaces\HttpMessageExceptionInterface;

/**
 * This exception can be used to catch more accurately
 * an invalid protocol exception thrown from a HttpMessage
 * object.
 * 
 * @author Côme Wasik <wasik.come@gmail.com>
 * @license https://opensource.org/licenses/MIT MIT
 */
class InvalidProtocolVersionException extends RuntimeException implements HttpMessageExceptionInterface
{
    // Redefine the exception so message isn't optional
    public function __construct($version, $code = 500, $previous = null)
    {
        parent::__construct('The protocol version "' . $version . '" does not respect the syntax <majorVersion>.<minorVersion>', $code, $previous);
    }
}
