<?php

namespace Nolikein\HttpMessage\Exception;

use Nolikein\HttpMessage\Interfaces\HttpMessageExceptionInterface;

/**
 * This exception can be used to catch more accurately
 * a header no found exception thrown from a HttpMessage
 * object.
 * 
 * @author Côme Wasik <wasik.come@gmail.com>
 * @license https://opensource.org/licenses/MIT MIT
 */
class HeaderNotFoundException extends RuntimeException implements HttpMessageExceptionInterface
{
    // Redefine the exception so message isn't optional
    public function __construct($headerName, $code = 500, $previous = null)
    {
        parent::__construct('The header named "' . $headerName . '" does not exist', $code, $previous);
    }
}
