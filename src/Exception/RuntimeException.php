<?php

namespace Nolikein\HttpMessage\Exception;

use Nolikein\HttpMessage\Interfaces\HttpMessageExceptionInterface;

/**
 * This exception can be used to catch more accurately
 * a runtime exception thrown from a HttpMessage object.
 * 
 * @author Côme Wasik <wasik.come@gmail.com>
 * @license https://opensource.org/licenses/MIT MIT
 */
class RuntimeException extends \RuntimeException implements HttpMessageExceptionInterface
{
}
