<?php

namespace Nolikein\HttpMessage;

use Nolikein\HttpMessage\Exception\InvalidArgumentException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\UriInterface;

/**
 * Request is a class which represent an http request.
 * It implements the Psr\RequestInterface.
 * 
 * @author Côme Wasik <wasik.come@gmail.com>
 * @license https://opensource.org/licenses/MIT MIT
 */
class Request extends Message implements RequestInterface
{
    /** @var const TARGET_ORIGIN The origin http target value */
    const TARGET_ORIGIN = 'origin-form';
    /** @var const TARGET_ABSOLUTE The absolute http target value */
    const TARGET_ABSOLUTE = 'absolute-form';
    /** @var const TARGET_AUTHORITY The authority http target value */
    const TARGET_AUTHORITY = 'authority-form';
    /** @var const TARGET_ASTERIX The asterisk http target value */
    const TARGET_ASTERIX = 'asterisk-form';

    /** Different http methods */
    const METHOD_GET = 'GET';
    const METHOD_HEAD = 'HEAD';
    const METHOD_POST = 'POST';
    const METHOD_PUT = 'PUT';
    const METHOD_UPDATE = 'UPDATE';
    const METHOD_DELETE = 'DELETE';
    const METHOD_CONNECT = 'CONNECT';
    const METHOD_OPTION = 'OPTION';
    const METHOD_TRACE = 'TRACE';
    const METHOD_PATCH = 'PATCH';

    const AVAILABLE_METHODS = [
        self::METHOD_GET,
        self::METHOD_HEAD,
        self::METHOD_POST,
        self::METHOD_PUT,
        self::METHOD_UPDATE,
        self::METHOD_DELETE,
        self::METHOD_CONNECT,
        self::METHOD_OPTION,
        self::METHOD_TRACE,
        self::METHOD_PATCH
    ];

    const AVAILABLE_TARGETS = [
        self::TARGET_ORIGIN,
        self::TARGET_ABSOLUTE,
        self::TARGET_AUTHORITY,
        self::TARGET_ASTERIX,
    ];

    /** @var string $requestTarget The current request target used in the request */
    protected $requestTarget = self::TARGET_ORIGIN;

    /** @var string $method The http method used in the request */
    protected $method = self::METHOD_GET;

    /** @var Uri $uri The uri used in the request */
    protected $uri;

    public function __construct()
    {
        $this->uri = new Uri();
    }

    public function getRequestTarget(): string
    {
        return $this->requestTarget;
    }

    /**
     * {@inheritDoc}
     *
     * There are 4 form which are allowed :
     * + origin-form => GET /where?q=now HTTP/1.1 ; Host: www.example.org
     * + absolute-form => GET http://www.example.org/pub/WWW/TheProject.html HTTP/1.1
     * + authority-form => CONNECT www.example.com:80 HTTP/1.1
     * + asterisk-form => OPTIONS * HTTP/1.1
     */
    public function withRequestTarget($requestTarget)
    {
        $clone = clone $this;

        if (!\is_string($requestTarget)) {
            throw new InvalidArgumentException('The request target must be a string', 500);
        }

        $clone->requestTarget = $requestTarget;
        return $clone;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function withMethod($method)
    {
        $clone = clone $this;

        if (!\is_string($method)) {
            throw new InvalidArgumentException('The http method must be a string', 500);
        }

        // While HTTP method names are typically all uppercase characters, HTTP method names are case-sensitive and thus implementations SHOULD NOT modify the given string.
        $method = strtoupper($method);

        if (!\in_array($method, self::AVAILABLE_METHODS)) {
            throw new InvalidArgumentException('The method ' . $method . ' is not allowed. Use instead : ' . implode(', ', self::AVAILABLE_METHODS), 400);
        }

        $clone->method = $method;
        return $clone;
    }

    public function getUri()
    {
        return $this->uri;
    }

    public function withUri(UriInterface $uri, $preserveHost = false)
    {
        $clone = clone $this;
        $clone->uri = $uri;

        if (
            (
                !$this->hasHeader('Host') && !empty($uri->getHost())
            ) || (
                !$preserveHost && $this->hasHeader('Host')
            )
            ) {
            $clone = $clone->withHeader('Host', $uri->getHost());
        }

        return $clone;
    }
}
