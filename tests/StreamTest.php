<?php

declare(strict_types=1);

namespace Test\Http;

use PHPUnit\Framework\TestCase;
use Nolikein\HttpMessage\Stream;
use Nolikein\Stream\Stream as BaseStream;

class StreamTest extends TestCase
{
    public function testParent()
    {
        $this->assertInstanceOf(BaseStream::class, (new Stream()));
    }
}
