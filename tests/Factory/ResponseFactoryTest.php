<?php

declare(strict_types=1);

namespace Test\Functionnal\Http\Factory;

use PHPUnit\Framework\TestCase;
use Nolikein\HttpMessage\Factory\ResponseFactory;

class ResponseFactoryTest extends TestCase
{
    public function testCreation()
    {
        $factory = new ResponseFactory();
        $resp1 = $factory->createResponse(404);
        $resp2 = $factory->createResponse(404, ResponseFactory::HTTP_MESSAGES[404]);

        $this->assertEquals(ResponseFactory::HTTP_MESSAGES[404], $resp1->getReasonPhrase(), 'The http message (reason phrase) of $resp1 is not correct');
        $this->assertEquals(ResponseFactory::HTTP_MESSAGES[404], $resp2->getReasonPhrase(), 'The http message (reason phrase) of $resp2 is not correct');

        $this->assertEquals(404, $resp1->getStatusCode(), 'The status code (http code) of $resp1 is not correct');
        $this->assertEquals(404, $resp1->getStatusCode(), 'The status code (http code) of $resp2 is not correct');

        $this->assertEquals(1.1, $resp1->getProtocolVersion());
        $this->assertEquals(1.1, $resp2->getProtocolVersion());

        $this->assertEmpty($resp1->getHeaders(), 'The response $resp1 MUST have no header');
        $this->assertEmpty($resp2->getHeaders(), 'The response $resp2 MUST have no header');

        $this->assertNull($resp1->getBody(), 'The response $resp1 MUST give an empty body');
        $this->assertNull($resp2->getBody(), 'The response $resp2 MUST give an empty body');
    }
}