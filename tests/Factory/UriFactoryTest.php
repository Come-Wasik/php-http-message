<?php

declare(strict_types=1);

namespace Test\Functionnal\Http\Factory;

use PHPUnit\Framework\TestCase;
use Nolikein\HttpMessage\Factory\UriFactory;

class UriFactoryTest extends TestCase
{
    public function testOne()
    {
        $uriFactory = new UriFactory();
        $uri = $uriFactory->createUri('https://username:password@mywebsite.org:502/my/path?param=value#main');

        $this->assertEquals('https', $uri->getScheme());
        $this->assertEquals('username:password', $uri->getUserInfo());
        $this->assertEquals(502, $uri->getPort());
        $this->assertEquals('/my/path', $uri->getPath());
        $this->assertEquals('param=value', $uri->getQuery());
        $this->assertEquals('main', $uri->getFragment());
    }
}