<?php

declare(strict_types=1);

namespace Test\Functionnal\Http\Factory;

use PHPUnit\Framework\TestCase;
use Nolikein\HttpMessage\Factory\ServerRequestFactory;
use Nolikein\HttpMessage\Factory\UriFactory;

class ServerRequestFactoryTest extends TestCase
{
    public function testCreation()
    {
        $serverRequestFactory = new ServerRequestFactory();
        $uriFactory = new UriFactory();
        $uri = $uriFactory->createUri('//mywebsite.org/my/path');
        $serverRequest = $serverRequestFactory->createServerRequest('GET', $uri);

        $this->assertEquals('GET', $serverRequest->getMethod());
        $this->assertEquals($uri, $serverRequest->getUri());
        $this->assertEquals('mywebsite.org', $serverRequest->getHeaderLine('host'), 'There MUST have the header host');
        $this->assertCount(1, $serverRequest->getHeaders(), 'There MUST have only one header, which is host');
        $this->assertEmpty($serverRequest->getServerParams());

        $serverRequest = $serverRequestFactory->createServerRequest('GET', $uri, ['data' => 'value']);
        $this->assertEquals(['data' => 'value'], $serverRequest->getServerParams(), 'There MUST have a serveur param');
    }
}
