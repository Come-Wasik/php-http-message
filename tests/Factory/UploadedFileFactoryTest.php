<?php

declare(strict_types=1);

namespace Test\Functionnal\Http\Factory;

use PHPUnit\Framework\TestCase;
use Nolikein\HttpMessage\Factory\StreamFactory;
use Nolikein\HttpMessage\Factory\UploadedFileFactory;

class UploadedFileFactoryTest extends TestCase
{
    public function testCreation()
    {
        $factory = new StreamFactory();
        $stream = $factory->createStreamFromFile('php://temp', 'r+');
        $factory = new UploadedFileFactory();

        # Filling only required params
        $upFile = $factory->createUploadedFile($stream);

        $this->assertNull($upFile->getSize());
        $this->assertEquals(\UPLOAD_ERR_OK, $upFile->getError());
        $this->assertNull($upFile->getClientFilename());
        $this->assertNull($upFile->getClientMediaType());


        # Filling all params
        $upFile = $factory->createUploadedFile($stream, 4, \UPLOAD_ERR_NO_TMP_DIR, 'myfile.txt', 'txt');
        $this->assertEquals(4, $upFile->getSize());
        $this->assertEquals(\UPLOAD_ERR_NO_TMP_DIR, $upFile->getError());
        $this->assertEquals('myfile.txt', $upFile->getClientFilename());
        $this->assertEquals('txt', $upFile->getClientMediaType());
    }
}
