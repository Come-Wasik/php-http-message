<?php

declare(strict_types=1);

namespace Test\Functionnal\Http\Factory;

use PHPUnit\Framework\TestCase;
use Nolikein\HttpMessage\Factory\RequestFactory;
use Nolikein\HttpMessage\Factory\UriFactory;

class RequestFactoryTest extends TestCase
{
    public function testCreation()
    {
        $factory = new RequestFactory();
        $request = $factory->createRequest('GET', 'https://username:password@mywebsite.org:502/my/path?param=value#main');

        $this->assertEquals('GET', $request->getMethod());
        $this->assertEquals('https', $request->getUri()->getScheme());
        $this->assertEquals('username:password', $request->getUri()->getUserInfo());
        $this->assertEquals('mywebsite.org', $request->getUri()->getHost());
        $this->assertEquals(502, $request->getUri()->getPort());
        $this->assertEquals('/my/path', $request->getUri()->getPath());
        $this->assertEquals('param=value', $request->getUri()->getQuery());
        $this->assertEquals('main', $request->getUri()->getFragment());

        $uriFactory = new UriFactory();
        $uri = $uriFactory->createUri('//mywebsite.org/my/path');
        $request = $factory->createRequest('POST', $uri);

        $this->assertEquals('POST', $request->getMethod());
        $this->assertEquals($uri, $request->getUri());
    }
}