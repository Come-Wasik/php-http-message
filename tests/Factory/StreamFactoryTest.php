<?php

declare(strict_types=1);

namespace Test\Functionnal\Http\Factory;

use Nolikein\HttpMessage\Factory\StreamFactory;
use PHPUnit\Framework\TestCase;
use Nolikein\Stream\StreamFactory as BaseStreamFactory;

class StreamFactoryTest extends TestCase
{
    public function testParent()
    {
        $this->assertInstanceOf(BaseStreamFactory::class, (new StreamFactory()));
    }
}