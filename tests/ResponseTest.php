<?php

declare(strict_types=1);

namespace Test\Http;

use Nolikein\HttpMessage\Exception\InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Nolikein\HttpMessage\Message;
use Nolikein\HttpMessage\Response;

class ResponseTest extends TestCase
{
    /**
     * Assert Http\Response is a child of the Http\Message object
     */
    public function testIsChildOfMessage()
    {
        $response = new Response();
        $this->assertInstanceOf(Message::class, $response);
    }

    /**
     * Assert the following methods work normally :
     * + withStatus
     * + getStatusCode
     * + getReasonPhrase
     */
    public function testResponseMethods()
    {
        $response = new Response();

        # Default values
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('', $response->getReasonPhrase());

        # Set new values
        $response->withStatus(404); // The second argument is not an obligation
        $response = $response->withStatus(404, 'Not found');

        $this->assertEquals(404, $response->getStatusCode());
        $this->assertEquals('Not found', $response->getReasonPhrase());
    }

    public function testStatusCodeException()
    {
        $response = new Response();

        # The status code must be an int
        $response->withStatus(404);

        $this->expectException(InvalidArgumentException::class);
        $response->withStatus('404');
    }

    public function testMessageException()
    {
        $response = new Response();

        # The status code must be an int
        $response->withStatus(404, 'Not found');

        $this->expectException(InvalidArgumentException::class);
        $response->withStatus(404, 22.1);
    }
}
