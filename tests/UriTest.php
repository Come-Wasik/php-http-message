<?php

declare(strict_types=1);

namespace Test\Http;

use Nolikein\HttpMessage\Exception\InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Nolikein\HttpMessage\Uri;

class uriTest extends TestCase
{
    public function testAllMethodsWhenitIsOkey()
    {
        $uri = new Uri();

        # With and get
        ## Scheme
        $this->assertEquals('', $uri->withScheme('')->getScheme());
        $this->assertEquals('http', $uri->withScheme('http')->getScheme());
        $this->assertEquals('http', $uri->withScheme('HtTp')->getScheme());

        ## User info
        $this->assertEquals('', $uri->withUserInfo('')->getUserInfo());
        $this->assertEquals('username', $uri->withUserInfo('username')->getUserInfo());
        $this->assertEquals('username', $uri->withUserInfo('username', null)->getUserInfo());
        $this->assertEquals('username:password', $uri->withUserInfo('username', 'password')->getUserInfo());

        ## Host
        $this->assertEquals('', $uri->withHost('')->getHost());
        $this->assertEquals('my-super-uri.fr', $uri->withHost('my-super-uri.fr')->getHost());
        $this->assertEquals('my-super-uri.fr', $uri->withHost('my-SUPER-uri.FR')->getHost());

        ## Port
        $this->assertNull($uri->withPort(null)->getPort());
        $this->assertEquals(18, $uri->withPort(18)->getPort());
        $this->assertEquals(18, $uri->withScheme('http')->withPort(18)->getPort());
        $this->assertNull($uri->withScheme('http')->withPort(80)->getPort());
        $this->assertNull($uri->withScheme('https')->withPort(443)->getPort());

        ## Path
        $this->assertEquals('', $uri->withPath('')->getPath());
        $this->assertEquals('/my/super/path', $uri->withPath('/my/super/path')->getPath());

        ## Query
        $this->assertEquals('', $uri->withQuery('')->getQuery());
        $this->assertEquals('data=value', $uri->withQuery('data=value')->getQuery());

        ## Fragment
        $this->assertEquals('', $uri->withFragment('')->getFragment());
        $this->assertEquals('hello', $uri->withFragment('hello')->getFragment());

        # Other methods
        $this->assertEquals('192.168.1.1', $uri->withHost('192.168.1.1')->getAuthority());
        $this->assertEquals('bob:alice@192.168.1.1', $uri->withScheme('http')->withUserInfo('bob', 'alice')->withHost('192.168.1.1')->withPort(80)->getAuthority());
        $this->assertEquals('bob:alice@192.168.1.1:80', $uri->withUserInfo('bob', 'alice')->withHost('192.168.1.1')->withPort(80)->getAuthority());
        $this->assertEquals('bob:alice@192.168.1.1:14', $uri->withUserInfo('bob', 'alice')->withHost('192.168.1.1')->withPort(14)->getAuthority());
    }

    public function testSchemeIsNotString()
    {
        $uri = new Uri();
        $this->expectException(InvalidArgumentException::class);
        $uri->withScheme(123);
    }

    public function testSchemeIsNotSuported()
    {
        $uri = new Uri();
        $this->expectException(InvalidArgumentException::class);
        $uri->withScheme('https3');
    }

    public function testUsernameIsNotString()
    {
        $uri = new Uri();
        $this->expectException(InvalidArgumentException::class);
        $uri->withUserInfo(123);
    }

    public function testPasswordIsNotStringOrNull()
    {
        $uri = new Uri();
        $this->expectException(InvalidArgumentException::class);
        $uri->withUserInfo('', 123);
    }

    public function testHostIsNotString()
    {
        $uri = new Uri();
        $this->expectException(InvalidArgumentException::class);
        $uri->withHost(123);
    }

    public function testHostIsNotValid()
    {
        $uri = new Uri();
        $this->expectException(InvalidArgumentException::class);
        $uri->withHost('www.truc.le\'fond.fr');
    }

    public function testPortIsNotInt()
    {
        $uri = new Uri();
        $this->expectException(InvalidArgumentException::class);
        $uri->withPort('123');
    }

    public function testPortIsGreatherThanTheMinimal()
    {
        $uri = new Uri();
        $this->expectException(InvalidArgumentException::class);
        $uri->withPort(-1);
    }

    public function testPortIsLessertThanTheMaximal()
    {
        $uri = new Uri();
        $this->expectException(InvalidArgumentException::class);
        $uri->withPort(65536);
    }

    public function testQueryIsNotString()
    {
        $uri = new Uri();
        $this->expectException(InvalidArgumentException::class);
        $uri->withQuery(123);
    }

    public function testQueryIsInvalid()
    {
        $uri = new Uri();
        $this->expectException(InvalidArgumentException::class);
        $uri->withQuery('truc');
    }

    public function testQueryIsEndedByAnd()
    {
        $uri = new Uri();
        $this->expectException(InvalidArgumentException::class);
        $uri->withQuery('truc=machin&');
    }

    public function testFragmentIsNotString()
    {
        $uri = new Uri();
        $this->expectException(InvalidArgumentException::class);
        $uri->withFragment(123);
    }

    public function testToString()
    {
        $uri = new Uri();

        # Checking __string
        ## Case where url is minimale
        $completeUriTest = $uri;
        ob_start();
        echo $completeUriTest;
        $output = ob_get_clean();
        $this->assertEquals('', $output);

        ## Case where url have a scheme
        $completeUriTest = $uri->withScheme('http');
        ob_start();
        echo $completeUriTest;
        $output = ob_get_clean();
        $this->assertEquals('http:', $output);

        ## Case where url have only an authority
        $completeUriTest = $uri
            ->withScheme('http')
            ->withUserInfo('myUsername', 'djJ7dKfgLù4')
            ->withHost('my-super-duper-host.fr')
            ->withPort(122);
        ob_start();
        echo $completeUriTest;
        $output = ob_get_clean();
        $this->assertEquals('http://myUsername:djJ7dKfgLù4@my-super-duper-host.fr:122', $output);

        ## Case where url have a scheme, a path rootless and no authority
        $completeUriTest = $uri
            ->withScheme('http')
            ->withPath('my/super/path');
        ob_start();
        echo $completeUriTest;
        $output = ob_get_clean();
        $this->assertEquals('http:my/super/path', $output);

        ## Case where url have a scheme, a path rootless but an authority
        $completeUriTest = $uri
            ->withScheme('http')
            ->withHost('my-super-duper-host.fr')
            ->withPath('my/super/path');
        ob_start();
        echo $completeUriTest;
        $output = ob_get_clean();
        $this->assertEquals('http://my-super-duper-host.fr/my/super/path', $output);

        ## Case where url have a scheme, and a full path and no authority
        $completeUriTest = $uri
            ->withScheme('http')
            ->withPath('///my/super/path');
        ob_start();
        echo $completeUriTest;
        $output = ob_get_clean();
        $this->assertEquals('http:/my/super/path', $output);

        ## Case where url is complete
        $completeUriTest = $uri
            ->withScheme('http')
            ->withPort(122)
            ->withUserInfo('myUsername', 'djJ7dKfgLù4')
            ->withHost('my-super-duper-host.fr')
            ->withPath('my/super/path')
            ->withQuery('truc=machin&essai=help')
            ->withFragment('hello');

        ob_start();
        echo $completeUriTest;
        $output = ob_get_clean();
        $this->assertEquals('http://myUsername:djJ7dKfgLù4@my-super-duper-host.fr:122/my/super/path?truc=machin&essai=help#hello', $output);
    }
}
