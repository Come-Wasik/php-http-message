<?php

declare(strict_types=1);

namespace Test\Http;

use PHPUnit\Framework\TestCase;
use Nolikein\HttpMessage\Exception\RuntimeException;
use Nolikein\HttpMessage\Stream;
use Nolikein\HttpMessage\UploadedFile;

class UploadedFileTest extends TestCase
{
    public function testAllMethods()
    {
        # Creating of a fake file which have fictive metadata
        $newFile = new Stream();
        $newFile->attach(tmpfile());
        $newFile->write('abc');
        $fakeFile = [
            'name' => 'myFile.txt',
            'type' => 'txt',
            'size' => $newFile->getSize(),
            'tmp_name' => $newFile->getMetadata('uri'),
            'error' => UPLOAD_ERR_OK
        ];

        # Testing methods
        ## Constructor
        $fileUpload = new UploadedFile();

        ## Setter
        $fileUpload->setFile($fakeFile);

        ## getters
        $this->assertEquals($fakeFile['size'], $fileUpload->getSize());
        $this->assertEquals($fakeFile['error'], $fileUpload->getError());
        $this->assertEquals($fakeFile['name'], $fileUpload->getClientFilename());
        $this->assertEquals($fakeFile['type'], $fileUpload->getClientMediaType());
        $this->assertInstanceOf(Stream::class, $fileUpload->getStream());

        ## "moveTo"
        $destFilePath = __DIR__ . '/cache/test.txt';
        $fileUpload->moveTo($destFilePath);
        $this->assertFileExists($destFilePath);

        ### Cannot use moveTo 2 times
        $this->expectException(RuntimeException::class);
        $fileUpload->moveTo($destFilePath);
        $this->assertTrue(unlink($destFilePath), 'The destination file cannot be deleted');
    }
}
