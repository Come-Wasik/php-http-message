<?php

declare(strict_types=1);

namespace Test\Http;

use Nolikein\HttpMessage\Exception\InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Psr\Http\Message\StreamInterface;
use Nolikein\HttpMessage\Exception\HeaderNotFoundException;
use Nolikein\HttpMessage\Exception\InvalidProtocolVersionException;
use Nolikein\HttpMessage\Message;
use Nolikein\HttpMessage\Stream;

class HttpMessageTest extends TestCase
{
    /**
     * getProtocolVersion - check the output format
     */
    public function testProtocol()
    {
        $message = new Message();
        $this->assertNull($message->getProtocolVersion(), 'The protocol version is not supposed to be defined by default');

        $message = $message->withProtocolVersion('002.005');
        $this->assertInstanceOf(Message::class, $message, 'The method withProtocolVersion do not return itself');
        $this->assertEquals('2.5', $message->getProtocolVersion(), 'The protocol version does not delete useless 0 in the protocol version or something else unknow is wrong');
    }

    /**
     * withProtocolVersion - Check exceptions
     */
    public function testProtocolThrowException()
    {
        $message = new Message();

        $message->withProtocolVersion('1.5');
        $message->withProtocolVersion(1.5);

        $this->expectException(InvalidArgumentException::class);

        $message->withProtocolVersion([]);
        $message->withProtocolVersion(2);

        $this->expectException(InvalidProtocolVersionException::class);

        $message->withProtocolVersion('2.5.4');
    }

    /**
     * Multiples tests around headers when it is suppose to works.
     *
     * List of tested functions :
     * + hasHeader
     * + withHeader
     * + getHeader
     * + getHeaders
     * + getHeaderLine
     * + withoutHeader
     */
    public function testHeader()
    {
        $message = new Message();
        $headerExamples = [
            'number1' => 'fake data 1',
            'number2' => 'fake data 2',
        ];

        # has header test (the header does not exist yet)
        $this->assertNotTrue($message->hasHeader('myHeader'));

        # Set one header
        $message = $message->withHeader('MyHeader', $headerExamples['number1']);

        # Has header test (the header is supposed existing) with normal case
        $this->assertTrue($message->hasHeader('myHeader'));
        # Has header test (the header is supposed existing) with different case
        $this->assertTrue($message->hasHeader('mYhEADeR'));

        # Get the header value with the exactly same case
        $this->assertIsArray($message->getHeader('MyHeader'));
        $this->assertEquals($headerExamples['number1'], $message->getHeader('MyHeader')[0]);

        # Gt the header value with a different case
        $this->assertEquals($headerExamples['number1'], $message->getHeader('mYhEAdEr')[0]);

        # Erase an existing header by another
        $message = $message->withHeader('MyHeader', $headerExamples['number2']);
        $this->assertCount(1, $message->getHeader('MyHeader'));
        $this->assertEquals($headerExamples['number2'], $message->getHeader('MyHeader')[0]);

        # Add a second header
        $message = $message->withAddedHeader('MyHeader', $headerExamples['number1']);
        $this->assertCount(2, $message->getHeader('MyHeader'));
        $this->assertEquals($headerExamples['number2'], $message->getHeader('MyHeader')[0]);
        $this->assertEquals($headerExamples['number1'], $message->getHeader('MyHeader')[1]);

        # Get the entire line
        $this->assertEquals(
            implode('; ', array_reverse($headerExamples)),
            $message->getHeaderLine('myHeader')
        );

        # Set another header and get the whole list
        $message = $message->withHeader('otherThing', $headerExamples['number1']);
        $allHeaders = $message->getHeaders();
        $this->assertIsArray($allHeaders, 'Message::getHeaders does not return an array');
        $this->assertArrayHasKey('myheader', $allHeaders, 'The added key "myheader" on below is not sended');
        $this->assertArrayHasKey('otherthing', $allHeaders, 'The added key "otherthing" on below is not sended');

        # Remove a header
        $message = $message->withoutHeader('myHeader');
        ## Verifying the header has been deleted
        $this->assertNotTrue($message->hasHeader('myHeader'));
    }

    /**
     * getHeader - Test exception when try to access on an inexisting element
     */
    public function testGetHeaderThrowException()
    {
        $message = new Message();

        $this->expectException(HeaderNotFoundException::class);
        $message->getHeader('it does not exist');
    }

    /**
     * getHeaderLine - Test exception when try to access on an inexisting element
     */
    public function testGetHeaderLineThrowException()
    {
        $message = new Message();

        $this->expectException(HeaderNotFoundException::class);
        $message->getHeaderLine('this too');
    }

    /**
     * withHeader - Check exceptions for the name
     */
    public function testWithHeaderInvalidNameException()
    {
        $message = new Message();

        # The name must be a string
        $message->withHeader('accepted value', 'accepted value');
        $this->expectException(InvalidArgumentException::class);

        $message->withHeader(12.0, 'accepted value');
    }

    /**
     * withHeader - Check exceptions for the value
     */
    public function testWithHeaderInvalidValueException()
    {
        $message = new Message();

        # The value must be a string or an array
        $message->withHeader('accepted value', 'accepted value');
        $message->withHeader('accepted value', ['accepted value']);

        $this->expectException(InvalidArgumentException::class);

        $message->withHeader('accepted value', 12.0);
    }

    /**
     * withAddedHeader exceptions by the name
     */
    public function testWithAddedHeaderInvalidNameException()
    {
        $message = new Message();

        # The name must be a string
        $message->withHeader('accepted value', 'accepted value');
        $this->expectException(InvalidArgumentException::class);

        $message->withAddedHeader(12.0, 'accepted value');
        $message->withAddedHeader([], 'accepted value');
    }

    /**
     * withAddedHeader exceptions by the value
     */
    public function testWithAddedHeaderInvalidValueException()
    {
        $message = new Message();

        # The value must be a string or an array
        $message->withAddedHeader('accepted value', 'accepted value');
        $message->withAddedHeader('accepted value', []);

        $this->expectException(InvalidArgumentException::class);

        $message->withHeader('accepted value', 12.0);
    }

    /**
     * Body - Check getBody and setBody
     */
    public function testBody()
    {
        $message = new Message();
        $stream = new Stream();
        $stream->attach(tmpfile());
        $streamContent = 'This is content';

        $this->assertNull($message->getBody());

        # Set content to the body
        $stream->write($streamContent);
        $message = $message->withBody($stream);

        # Assert the body has a StreamInterface object
        $this->assertNotNull($message->getBody());
        $this->assertInstanceOf(StreamInterface::class, $message->getBody());

        # Read the content from the body
        ob_start();
        echo $message->getBody();
        $bodyContent = ob_get_clean();

        $this->assertEquals($streamContent, $bodyContent);
    }
}
