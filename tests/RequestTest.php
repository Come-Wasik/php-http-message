<?php

declare(strict_types=1);

namespace Test\Http;

use Nolikein\HttpMessage\Exception\InvalidArgumentException;
use PHPUnit\Framework\TestCase;
use Nolikein\HttpMessage\Message;
use Nolikein\HttpMessage\Request;
use Nolikein\HttpMessage\Uri;

class RequestTest extends TestCase
{
    /**
     * Assert Http\Request is a child of the Http\Message object
     */
    public function testIsChildOfMessage()
    {
        $request = new Request();
        $this->assertInstanceOf(Message::class, $request);
    }

    public function testDefaultValues()
    {
        $request = new Request();

        $this->assertEquals(Request::METHOD_GET, $request->getMethod());
        $this->assertEquals(Request::TARGET_ORIGIN, $request->getRequestTarget());
    }

    /**
     * Testing the conform using of the Request objet
     */
    public function testSetValues()
    {
        $request = new Request();

        # Method
        ## Constant, string and case equality
        $this->assertEquals(Request::METHOD_POST, $request->withMethod(Request::METHOD_POST)->getMethod());
        $this->assertEquals(Request::METHOD_POST, $request->withMethod('POST')->getMethod());
        $this->assertEquals(Request::METHOD_POST, $request->withMethod('post')->getMethod());

        ## Testing all provided methods
        foreach (Request::AVAILABLE_METHODS as $curMethod) {
            $this->assertEquals($curMethod, $request->withMethod($curMethod)->getMethod());
        }

        # Target
        ## Constant and string equality
        $this->assertEquals(Request::TARGET_ABSOLUTE, $request->withRequestTarget(Request::TARGET_ABSOLUTE)->getRequestTarget());
        $this->assertEquals(Request::TARGET_ABSOLUTE, $request->withRequestTarget('absolute-form')->getRequestTarget());

        ## Testing all provided targets
        foreach (Request::AVAILABLE_TARGETS as $curTarget) {
            $this->assertEquals($curTarget, $request->withRequestTarget($curTarget)->getRequestTarget());
        }
    }

    public function testMethodBadTypeException()
    {
        $request = new Request();

        $this->expectException(InvalidArgumentException::class);
        $request->withMethod(12);
    }

    public function testTargetBadTypeException()
    {
        $request = new Request();

        $this->expectException(InvalidArgumentException::class);
        $request->withRequestTarget(12);
    }

    public function testHttpMethodNotAllowed()
    {
        $request = new Request();

        $this->expectException(InvalidArgumentException::class);
        $request->withMethod('NOT_ALLOWED');
    }

    public function testSetUri()
    {
        $request = new Request();
        $this->assertFalse($request->withUri(new Uri())->hasHeader('Host'));
        $this->assertEquals('my-host.fr', $request->withUri((new Uri())->withHost('my-host.fr'))->getHeaderLine('Host'));
        $this->assertEquals('my-host.fr', $request->withHeader('Host', 'fake-host.fr')->withUri((new Uri())->withHost('my-host.fr'))->getHeaderLine('Host'));
        $this->assertEquals('fake-host.fr', $request->withHeader('Host', 'fake-host.fr')->withUri((new Uri())->withHost('my-host.fr'), true)->getHeaderLine('Host'));
    }
}
