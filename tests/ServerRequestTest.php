<?php

declare(strict_types=1);

namespace Test\Http;

use Nolikein\Cookie\CookieFactory;
use PHPUnit\Framework\TestCase;
use Nolikein\HttpMessage\Request;
use Nolikein\HttpMessage\ServerRequest;
use Nolikein\HttpMessage\Stream;
use Nolikein\HttpMessage\UploadedFile;

class ServerRequestTest extends TestCase
{
    public function testBelongToRequest()
    {
        $serverRequest = new ServerRequest();
        $this->assertInstanceOf(Request::class, $serverRequest);
    }

    public function testMethods()
    {
        $newFile = new Stream();
        $newFile->attach(tmpfile());
        $newFile->write('abc');
        $fakeFile = [
            'name' => 'myFile.txt',
            'type' => 'txt',
            'size' => $newFile->getSize(),
            'tmp_name' => $newFile->getMetadata('uri'),
            'error' => UPLOAD_ERR_OK
        ];
        $uploadedFileData = [new UploadedFile($fakeFile)];

        $serverData = ['REQUEST_URI' => '/mypath'];
        $queryData = ['hello' => 'home', 'hallo' => 'meine'];
        $postData = ['bodytext' => 'world'];
        $cookieData = [(new CookieFactory)->createCookie('mycookie', 'testvalue')];

        $serverRequest = new ServerRequest();
        # Creating a blank request
        $serverRequest = $serverRequest->withQueryParams($queryData)
            ->withParsedBody($postData)
            ->withCookieParams($cookieData)
            ->withServerParams($serverData)
            ->withUploadedFiles($uploadedFileData);

        # Test each get methods
        $this->assertEquals($queryData, $serverRequest->getQueryParams());
        $this->assertEquals($postData, $serverRequest->getParsedBody());
        $this->assertEquals($cookieData, $serverRequest->getCookieParams());
        $this->assertEquals($serverData, $serverRequest->getServerParams());
        $this->assertEquals($uploadedFileData, $serverRequest->getUploadedFiles());
    }

    public function testAttributeMethods()
    {
        $serverRequest = new ServerRequest();
        # Test with nothing, then affect data, test if the affected data exist, finally delete and test removed data
        $this->assertEmpty($serverRequest->getAttributes());
        $serverRequest = $serverRequest->withAttribute('hello', 'world');
        $this->assertEquals('world', $serverRequest->getAttributes()['hello']);
        $this->assertEquals('world', $serverRequest->getAttribute('hello'));
        $serverRequest = $serverRequest->withoutAttribute('hello');
        $this->assertEmpty($serverRequest->getAttributes());
        $this->assertNull($serverRequest->getAttribute('hello'));
    }
}
